import React from "react";
import { Fruta } from "./components/fruta";

class App extends React.Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ];

  render() {
    return (
      <div className="App">
        <div id="all fruit names">
          <Fruta
            fruits={this.fruits.map((fruit, index) => (
              <li key={index}>{fruit.name} </li>
            ))}
          />
        </div>
        <div id="red fruit names">
          <Fruta
            fruits={this.fruits
              .filter((fruit) => fruit.color === "red")
              .map((fruit, index) => (
                <li key={index}>{fruit.name} </li>
              ))}
          />
        </div>
        <div id="total">
          <Fruta
            fruits={this.fruits.reduce((acc, item) => acc + item.price, 0)}
          ></Fruta>
        </div>
      </div>
    );
  }
}

export default App;
